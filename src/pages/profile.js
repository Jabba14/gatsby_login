import React from "react"
import Layout from "../components/layout"
import Profile from "../components/profile"

const ProfilePage = ({ location }) => (
  <Layout location={location}>
    <Profile />
  </Layout>
)

export default ProfilePage

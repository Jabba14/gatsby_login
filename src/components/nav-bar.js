import React from "react"
import { Link } from "gatsby"
import { getUser, isLoggedIn, logout } from "../services/auth"

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      greetingMessage: ""
    }
  }

  componentDidMount() {
    let greetingMessage = ""
    if (isLoggedIn()) {
      greetingMessage = `ハロー ${getUser().name} さん`
    } else {
      greetingMessage = "You are not logged in"
    }
    this.setState({
      greetingMessage: greetingMessage
    })
  }

  logout = (event) => {
    event.preventDefault();
    this.setState({
      greetingMessage: "You are not logged in"
    })
  }

  render() {
    return(
      <div
        style={{
          display: "flex",
          flex: "1",
          justifyContent: "space-between",
          borderBottom: "1px solid #d1c1e0",
        }}
      >
        <span>{this.state.greetingMessage}</span>

        <nav>
          <Link to="/">Home</Link>
          {` `}
          {isLoggedIn()
            ? (
              <>
                <Link to="/profile">Profile</Link>
                {` `}
                <Link to="/" onClick={logout} >
                  Logout
                </Link>
              </>
            )
            : (
              <Link to="/login">Login</Link>
            )
          }
        </nav>
      </div>
    )
  }
}

export default NavBar

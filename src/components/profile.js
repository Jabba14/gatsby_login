import React from "react"
import { navigate } from "gatsby"
import { getUser, isLoggedIn } from "../services/auth"

class Profile extends React.Component {
  componentDidMount() {
    if (!isLoggedIn()) {
      navigate(`/login`)
    }
  }

  render() {
    return (
      <>
        <h1>Your profile</h1>
        <ul>
          <li>Name: {getUser().name}</li>
          <li>E-mail: {getUser().email}</li>
        </ul>
      </>
    )
  }
}

export default Profile
